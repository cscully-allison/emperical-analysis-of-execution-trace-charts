import json
import sys
import random
import copy
import math

que_spec_file = None
debug = False
paper_prototype = False
tutorial_questions = False

for i, arg in enumerate(sys.argv):
    if arg == '-f':
        que_spec_file = sys.argv[i+1]
    if arg == '-d':
        debug = True
    if arg == '-p':
        paper_prototype = True
    if arg == '-t':
        tutorial_questions = True

def prettyPrintQuestions(questions, filename):
    q_string = json.dumps(questions)
    nesting = 0
    quote = False
    list = False
    liststate = False
    nested_list = False
    last = ''

    with open(filename, mode='w', newline='') as f:
        for char in q_string:
            if char not in '{},[]\n\t\r' and not char.isspace():
                f.write(char)

            if char in '"':
                quote = not quote

            elif char.isspace() and quote:
                f.write(char)

            elif char in ',':
                f.write(char)
                if quote:
                    pass
                elif last in '}':
                    f.write('\n')
                    for indent in range(0, nesting):
                        f.write('\t')
                elif not list:
                    f.write('\n')
                    for indent in range(0, nesting):
                        f.write('\t')

            elif char in '{':
                liststate = list
                list = False
                f.write(char+'\n')
                nesting += 1;
                for indent in range(0, nesting):
                    f.write('\t')

            elif char in '}':
                f.write('\n')
                nesting -= 1
                for indent in range(0, nesting):
                    f.write('\t')
                f.write(char)

                list = liststate

            elif char in '[':
                nesting += 1
                if list is True:
                    f.write('\n')
                    for indent in range(0, nesting):
                        f.write('\t')
                    nested_list = True
                else:
                    list = True

                f.write(char)

            elif char in ']':
                nesting -= 1

                if last in '}]':
                    f.write('\n')
                    for indent in range(0, nesting):
                        f.write('\t')

                f.write(char)

                if nested_list is True:
                    nested_list = False
                else:
                    list = False


            last = char

def buildMessages(pattern, grid_size, off_cells, offset, adl_config):
    # build array of connections
    # again assume left to right
    msgs =  []

    # case for basic offset
    if "offset" in pattern:
        if offset is 0:
            off_cells = [grid_size[0]*grid_size[1]]
        for i in range(0, off_cells[len(off_cells)-1], grid_size[1]): #increment by grid size width steps
            receiver_index = i + (grid_size[1] - 1) + (grid_size[1] * offset) #calculate the corresponding message reciever based on offset
            new_msg = []
            new_msg.append(i)
            new_msg.append(receiver_index)
            msgs.append(new_msg)

    elif "ring" in pattern:
        wrap_msgs = deriveOffCells(grid_size, offset)

        for i in range(0, wrap_msgs[len(wrap_msgs)-1], grid_size[1]):
            receiver_index = i + (grid_size[1] - 1) + (grid_size[1] * offset) #calculate the corresponding message reciever based on offset
            new_msg = []
            new_msg.append(i)
            new_msg.append(receiver_index) #what are your offset secrets
            msgs.append(new_msg)

        for i in range(0, int(len(wrap_msgs)/2)):
            new_msg = []
            new_msg.append(wrap_msgs[len(wrap_msgs)-i-1])
            new_msg.append(wrap_msgs[i])
            msgs.append(new_msg)

    elif "stencil" in pattern:
        # construct mpi representation shape
        #  but first test that the domain height and drawing heights are evenly divisible
        try:
            if not(grid_size[0]/adl_config['mpi_domain_height'] == math.floor(grid_size[0]/adl_config['mpi_domain_height'])):
                raise Exception("Stencil Specification Error: The first value in \"grid_size\" must be evenly divisible by \"mpi_domain_height\".")
        except Exception as inst:
            print(inst.args)
            raise

        h = adl_config['mpi_domain_height'];
        w = int(grid_size[0]/h);

        #   iterate over mpi representation
        #   and store connections
        for row in range(0, h):
            for col in range(0, w):
                gid = col+(row*w)
                sender = gid*2
                if col is not 0: #leftmost col
                    reciever = gid-1
                    reciever = reciever*2 + 1
                    msgs.append([sender, reciever])
                if row is not 0: #topmost row
                    reciever = gid - w
                    reciever = reciever*2 + 1
                    msgs.append([sender, reciever])
                if col is not w-1: #rightmost col
                    reciever = gid + 1
                    reciever = reciever*2 + 1
                    msgs.append([sender, reciever])
                if row is not h-1: #bottommost row
                    reciever = gid + w
                    reciever = reciever*2 + 1
                    msgs.append([sender, reciever])
                if offset is 9:
                    if (col is not 0) and (row is not 0): #left topmost cell
                        reciever = (gid - 1) - w
                        reciever = reciever*2 + 1
                        msgs.append([sender, reciever])
                    if (row is not 0) and (col is not w-1): #top rightmost cell
                        reciever = (gid - w) + 1
                        reciever = reciever*2 + 1
                        msgs.append([sender, reciever])
                    if (col is not w-1) and (row is not h-1): #right bottommost cell
                        reciever = (gid + 1) + w
                        reciever = reciever*2 + 1
                        msgs.append([sender, reciever])
                    if (row is not h-1) and (col is not 0): #bottom leftmost cell
                        reciever = (gid + w) - 1  
                        reciever = reciever*2 + 1
                        msgs.append([sender, reciever])


    elif "exchange" in pattern:
        try:
            if not(grid_size[0]/(offset*2) == math.floor(grid_size[0]/(offset*2))):
                raise Exception("Exchange Specification Error: The first value in \"grid_size\" must be evenly divisible by two times\"offset\".")
        except Exception as inst:
            print(inst.args)
            raise

        for i in range(0, grid_size[0], offset*2):
            top = i*grid_size[1]
            end = top+(offset*grid_size[1]*2)
            midpoint = int((top+end)/2)
            reciever = midpoint + 1
            for sender in range(top, midpoint, grid_size[1]):
                msgs.append([sender, reciever])
                reciever += grid_size[1]

            reciever = top + 1
            for sender in range(midpoint, end, grid_size[1]):
                msgs.append([sender, reciever])
                reciever += grid_size[1]

    return msgs

def deriveOffCells(grid_size, offset):
    temp = []

    # assume left to right now
    endpt = (grid_size[0] * grid_size[1]) - grid_size[1]

    # calc top off cells
    for i in range(grid_size[1]-1, (offset*grid_size[1]), grid_size[1]):
        temp.append(i)

    # calc bottom off_cells
    for i in range(endpt, endpt-(offset*grid_size[1]), -grid_size[1]):
        temp.append(i)

    return temp

def normalizeMessages(messages, off_cells, base):
    subtractor = base
    normalMsgs = []
    normalOff = []

    for i, messagepairs in enumerate(messages):
        msg = []
        for j, message in enumerate(messagepairs):
            message -= subtractor
            msg.append(message)
        normalMsgs.append(msg)


    for i, off in enumerate(off_cells):
        normalOff.append(off - subtractor)

    return (normalMsgs, normalOff)

def getMsgsInRange(messages, range):
    msgs = []
    for messagepair in messages:
        # add all lines in our view
        if messagepair[0] >= range[0] and messagepair[0] < range[1]:
            msgs.append(messagepair)
        elif messagepair[1] >= range[0] and messagepair[1] < range[1]:
            msgs.append(messagepair)
        # append any lines which would overlap our view
        elif messagepair[0] < range[0] and messagepair[1] > range[1]:
            msgs.append(messagepair)
        elif messagepair[1] < range[0] and messagepair[0] > range[1]:
            msgs.append(messagepair)
    return msgs

def getPartialSlice(messages, slice_loc, slice_size, height):
    slice_size = slice_size*2
    try:
        if 'top' in slice_loc:
            range = [0,slice_size]
            return (getMsgsInRange(messages, range), 0)
        elif 'middle' in slice_loc:
            midpoint = math.floor(height)
            midpoint -= math.floor(slice_size/2)
            range = [midpoint, midpoint + slice_size]
            return (getMsgsInRange(messages, range), midpoint)
        elif 'bottom' in slice_loc:
            end = height*2
            range = [end-slice_size, end]
            return (getMsgsInRange(messages, range), (end-slice_size))
        else:
            raise Exception('Invalid slice_loc specification, it must be either \"top\", \"middle\", or \"bottom\".')
    except Exception:
        raise

def groupMessages(msgs, off_cells, addtl_configs, grid_size, pattern, offset):

    try:
        group = addtl_configs['grouping']
        if not( grid_size[0]/(group+offset) == math.floor(grid_size[0]/(group+offset))) and not(offset is 0):
            raise Exception("Message grouping specification error. The first integer of grid size must be evenly divisble by grouping number plus offset.")

        basecells = deriveOffCells([group+offset, 2], offset)


        if offset is 0:
            basecells = [group*2, group*2+1]
            newoffcells = []
            for i in range(0, grid_size[0]*grid_size[1], (group*2) + grid_size[1]):
                cellgrp = []
                for cell in basecells:
                    cellgrp.append(cell+i)
                newoffcells.append(cellgrp)
        else:
            newoffcells = []
            for i in range(0, grid_size[0]*grid_size[1], (group+offset)*grid_size[1]):
                cellgrp = []
                for cell in basecells:
                    cellgrp.append(cell+i)
                newoffcells.append(cellgrp)
        
        # remove connected msgs
        # just atrocious algorithm
        for cellgrp in newoffcells:
            for cell in cellgrp:
                for pair in msgs:
                    if cell in pair:
                        msgs.remove(pair)

        off_cells = []
        if 'offset' in pattern:
            for cellgrp in newoffcells:
                for cell in cellgrp:
                    off_cells.append(cell)


        if 'ring' in pattern:
            # remove crossing back lines every nth line
            rmv = deriveOffCells(grid_size, offset)
            for cell in rmv:
                for pair in msgs:
                    if pair[0] is cell or pair[1] is cell:
                        msgs.remove(pair)

            for wrap_msgs in newoffcells:
                for i in range(0, int(len(wrap_msgs)/2)):
                    new_msg = []
                    new_msg.append(wrap_msgs[len(wrap_msgs)-i-1])
                    new_msg.append(wrap_msgs[i])
                    msgs.append(new_msg)

        return msgs, off_cells

    except Exception:
        raise

def mirrorMessages(msgs, off_cells):
    messages = []
    off = []
    for messagepair in msgs:
        messages.append([ messagepair[1]-1, messagepair[0]+1])

    for cell in off_cells:
        if cell%2 is 1:
            off.append(cell-1)
        else:
            off.append(cell+1)

    return messages, off

def buildPattern(pattern, grid_size, structure, offset, addtl_configs = None):
    question_sample = {}
    # encode grid grid_size
    question_sample['grid_size'] = copy.deepcopy(grid_size)
    question_sample['off_cells'] = []

    # fork for offset & exchnage vs ring and stencil
    # ring and stencil do not have off cells unless grouped
    if 'offset' in pattern:
        # calc off cells
        question_sample['off_cells'] = deriveOffCells(grid_size, offset)

    question_sample['messages'] = buildMessages(pattern, grid_size, question_sample['off_cells'], offset, addtl_configs)
   
    # HANDLE POST GENERATION CONFIGURATIONS

    #grouping
    if addtl_configs is not None and 'grouping' in addtl_configs and not('exchange' in pattern):
        question_sample['messages'], question_sample['off_cells'] = groupMessages(question_sample['messages'], question_sample['off_cells'], addtl_configs, grid_size, pattern, offset)

    # mirror
    if addtl_configs is not None and 'mirror' in addtl_configs:
        if addtl_configs['mirror'] is 1:
            question_sample['messages'], question_sample['off_cells'] = mirrorMessages(question_sample['messages'], question_sample['off_cells'])

    # slice partial
    if 'partial' in structure:
        slice_size = 8

        msgs, base = getPartialSlice(question_sample['messages'], addtl_configs['partial_loc'], slice_size, grid_size[0])
        question_sample['messages'], question_sample['off_cells'] = normalizeMessages(msgs, question_sample['off_cells'], base)

        question_sample['grid_size'][0] = 8

    return question_sample

def addHintText(question):
    qt = question['question_type']
    choices = question['choices'][0]
    
    if qt == 'direct comparison':
        if question['offset'] == choices['offset']:
            question['hint'] = '''These two patterns have the same stride. 
                                If possible, try to count the many rows are between the beginning and end of a line. 
                                If a chart is very dense and you cannot see the rows clearly, try to estimate. 
                                Remember also that grouped and continious patterns do not have the same stride by default. 
                                This is also true for patterns where the slope between the two charts go in opposite directions (up from left-to-right vs. down from left-to-right).'''
        else:
            question['hint'] = '''These two patterns do not have the same stride. The pattern on the left has a stride of {} and the pattern on the right has a stride of {}. 
                                If possible, try to count the many rows are between the beginning and end of a line. 
                                If a chart is very dense and you cannot see the rows clearly, try to estimate. 
                                Remember also that grouped and continious patterns do not have the same stride by default. 
                                This is also true for patterns where the slope between the two charts go in opposite directions (up from left-to-right vs. down from left-to-right).'''.format(question['offset'], choices['offset'])
    elif qt == 'categorization':
        if choices['type'] == 'grouping':
            question['hint'] =  "This pattern is {}. Remember to look for gaps or breaks in a pattern indicate that it is \"grouped\", otherwise it is \"continuous\".".format(choices['correct'])
        elif choices['type'] == 'pattern':
            question['hint'] =  "This pattern is a {} pattern.".format(choices['correct'])
            if choices['correct'] == "offset":
                question['hint'] += " Remember that an offset is just a simple repetition of sloped lines."
            elif choices['correct'] == "ring":
                    question['hint'] += " Remember that a ring pattern looks like the offset pattern, but has one or multiple lines running perpendicular to the majority."
            elif choices['correct'] == "exchange":
                question['hint'] += " Remember that an exchange pattern has an equal number of lines criss-crossing eachother."
            

    return question

def buildQuestionJson(conf):
    output_question = {}
    config = conf["question"]
    question_structure = conf['question_structure']

    pattern = config['pattern']
    if 'stencil' in pattern:
        term = 'point'
    else:
        term = 'offset'
    offset = config['offset']
    grid_size = config['grid_size']
    adl_configs = {}

    if 'addtl_configs' in config:
        adl_configs = config['addtl_configs']

    if 'grouping' in adl_configs:
        adl_configs['pattern'] = 'grouped'
    else:
        adl_configs['pattern'] = 'continuous'


    # indicate question type as direct comparison
    if conf['question_type'] == 'dc':
        output_question['question_type'] = 'direct comparison'
    elif conf['question_type'] == 'tf':
        output_question['question_type'] = 'true/false'
    elif conf['question_type'] == 'cat':
        output_question['question_type'] = 'categorization'

    # generate note
    # offset_num term pattern subtype
    output_question['note'] = "{0} {1}, {2} {3}".format(offset, term, pattern, adl_configs["pattern"])
    output_question['offset'] = offset
    output_question['pattern'] = pattern
    output_question['other_characteristics'] = adl_configs
    output_question['representation'] = question_structure["question"]
    output_question['abstract'] = config['abstract']

    # question_text
    if conf['question_type'] == 'dc':
        if paper_prototype is False:
            output_question['question_text'] = "Does the {0} chart on the right have the same <b>stride</b> as the {1} chart on the left.".format(question_structure["question"], question_structure["comparison"])
        else:
            if "full" in question_structure["question"]:
                output_question['question_text'] = "The image on the left shows a full pattern of repeating lines. Each line connects two boxes. To the best of your ability, please draw a pattern equivalent to the full pattern within the 12 rows of boxes on the right. Additionally, using the provided crayons, please color the boxes to match this pattern of boxes on the left."
            if "partial" in question_structure["question"]:
                output_question['question_text'] = "The image on the left shows the {} excerpt of a larger repeating pattern. To the best of your ability, please draw your best guess for what this pattern would look like over the 12 rows of boxes on the right. Additionally, using the provided crayons, please color the boxes to match this pattern of boxes on the left.".format(adl_configs["partial_loc"])
    if conf['question_type'] == 'cat':
        output_question['question_text'] = "Please click on the corresponding category which best describes the line pattern on the left."

    # question_sample
    question_sample = {}

    # encode grid grid_size
    question_sample['grid_size'] = grid_size

    output_question['question_sample'] = buildPattern(pattern, grid_size, question_structure["question"], offset, adl_configs)


    return output_question


def buildComparaisionsJson(config, question):
    questions = []
    # load config in variables
    question_structure = config['question_structure']
    comparisons = config['comparisons']
    num_comps = comparisons['number']
    num_correct = comparisons['num_correct']
    incrct = comparisons['incorrect']
    correct = comparisons['correct']

############# correct JSONS
    for i in range(0, num_correct):
        q_n = copy.deepcopy(question)
        adl_config = {}
        c_question = {}

        pattern = config['question']['pattern']
        offset = config['question']['offset']
        
        grid_size = correct['grid_size'][i%len(correct['grid_size'])] #loops over defined grid
        
                
        if 'addtl_configs' in correct:
            adl_config = copy.deepcopy(correct['addtl_configs'][i])


        # load initial additional configuarions
        # if 'addtl_configs' in config['question']:
        #     adl_config = config['question']['addtl_configs']
        # adl_config['pattern'] = 'continuous'

        # if 'addtl_configs' in correct:
        #     if "partial_loc" in correct['addtl_configs']:
        #         slice_locs = correct['addtl_configs']['partial_loc']
        #         adl_config['partial_loc'] = slice_locs[i%len(slice_locs)]

        if 'addtl_configs' in config['question']:
            if "mirror" in config['question']['addtl_configs']:
                mirr = config['question']['addtl_configs']['mirror']
                adl_config['mirror'] = mirr

        #     if "grouping" in config['question']['addtl_configs']:
        #         grp = config['question']['addtl_configs']['grouping']
        #         adl_config['grouping'] = grp
        #         adl_config["pattern"] = "grouped"


        adl_config['pattern'] = 'continuous'
        if 'grouping' in adl_config:
            adl_config['pattern'] = 'grouped'


        c_question = buildPattern(pattern, grid_size, question_structure['comparison'], offset, adl_config)

        # generate note
        # offset_num term pattern subtype
        if 'stencil' in pattern:
            term = 'point'
        else:
            term = 'offset'
        c_question['note'] = "{0} {1}, {2} {3}".format(offset, term, pattern, adl_config["pattern"])
        c_question['offset'] = offset
        c_question['pattern'] = pattern
        c_question['correct'] = "yes"
        c_question['other_characteristics'] = copy.deepcopy(adl_config)
        c_question['representation'] = question_structure['comparison']
        c_question['abstract'] = correct['abstract'][i%len(correct['abstract'])]

    

        q_n["choices"] = [c_question]
        q_n["question_id"] = '{}C{}'.format(q_n['debug_id'], i)

        questions.append(q_n)

########### Incorrect JSONS

    for i in range(0, (num_comps - num_correct)):
        ic_question = {}
        q_n = copy.deepcopy(question)
        adl_config = {"pattern": "continuous"}

        # we need combinations of these specifications
        pattern = incrct["pattern"][i%len(incrct['pattern'])]
        grid_size = incrct['grid_size'][i%len(incrct['grid_size'])]

        # specify grouping characteristics conditionally
        if 'mpi_domain_height' in incrct['addtl_configs']:
            adl_config['mpi_domain_height'] = incrct['addtl_configs']['mpi_domain_height'][i%len(incrct['addtl_configs']['mpi_domain_height'])]

        offset = incrct['offset'][i%len(incrct['offset'])]

        # handle postgeneration configurations
        # partial location, mirroring and grouping
        if 'addtl_configs' in incrct:
            adl_config = copy.deepcopy(incrct['addtl_configs'][i])

        adl_config['pattern'] = 'continuous'
        if 'grouping' in adl_config:
            adl_config['pattern'] = 'grouped'


        # build before assignment
        ic_question = buildPattern(pattern, grid_size, question_structure['comparison'], offset, adl_config)

        # note
        # generate note
        if 'stencil' in pattern:
            term = 'point'
        else:
            term = 'offset'

        ic_question['note'] = "{0} {1}, {2} {3}".format(offset, term, pattern, adl_config["pattern"])
        ic_question['offset'] = offset
        ic_question['pattern'] = pattern
        ic_question['correct'] = "no"
        ic_question['other_characteristics'] = adl_config
        ic_question['representation'] = question_structure['comparison']
        ic_question['abstract'] = incrct['abstract'][i%len(incrct['abstract'])]

        q_n["choices"] = [ic_question]
        q_n["question_id"] = '{}I{}'.format(q_n['debug_id'], i)

        # eliminate duplicate questions
        duplicate = False
        for q in questions:
            if q == q_n:
                duplicate = True

        if not duplicate:
            questions.append(q_n)


    return questions



def main():
    config = None

    with open(que_spec_file, 'r') as f:
        configs = json.loads(f.read())

    questions = []
    for i, config in enumerate(configs):
        print("*"*100)
        print("Building question ", i)
        # # build question defintion json
        question = buildQuestionJson(config)
        question['debug_id'] = i
        #build comparison(s) json
        
        if config['question_type'] == 'dc':
            print("Building comparision charts for question ", i)
            for q in buildComparaisionsJson(config, question):
                questions.append(q)

        elif config['question_type'] == 'cat':
            print("*"*100)

            qp = copy.deepcopy(question)
            qp['choices'] = []
            qp['choices'].append({
                'type': 'pattern',
                'correct': qp['pattern']
            })
            qp["question_id"] = '{}P'.format(qp['debug_id'])

            qgrp = copy.deepcopy(question)
            qgrp['choices'] = []
            qgrp['choices'].append({
                'type': 'grouping',
                'correct': qgrp['other_characteristics']['pattern']
            })
            qgrp["question_id"] = '{}G'.format(qgrp['debug_id'])


            questions.append(qp)
            questions.append(qgrp)

    output = {}
    if tutorial_questions == True:
        for q in questions:
            q["question_id"] = "T" + q["question_id"]

            q = addHintText(q)

        output['tutorial'] = questions
        print("Outputting questions to file.")
        prettyPrintQuestions(output, 'full_tutorial_questions.json')
    else:
        output['questions'] = questions
        print("Outputting questions to file.")
        prettyPrintQuestions(output, 'full_test_questions.json')


    

    if debug:
        with open('debug.json', 'w') as f:
            f.write(json.dumps(questions))

    pass


main()
