import json
import glob
from collections import deque

ANSWER_KEY1 = None
ANSWER_KEY2 = None
TRACK_ANSWERS = {}
LOWER_BOUNDS = 1510750000
UPPER_BOUNDS = 1510760000

PARTICIPANT_LIST = { 'num_entries' : 0 }

def read_file_group(files):
    counter = 0
    for file_name in files:       
        file = open(file_name,"r")
        content = file.read()
        add_participant_entries(content)
        counter+=1
        file.close()
    return counter

def add_participant_entries(content):

    global PARTICIPANT_LIST

    session_id = float(content.split("session : ")[1].split(".txt")[0])
    entry = { 'session' : session_id }

    list = content.split("--")
    for x in range(0,len(list)):

        temp_list = list[x].split("\n")
        if x == 0:
            key='bio'
        else:
            key = str(x)
        
        entry[key] = {}

        for i in range(0,len(temp_list)):
            if len(temp_list[i].split(" : "))>1:
                entry[key][temp_list[i].split(" : ")[0].strip()] = return_type(temp_list[i].split(" : "), temp_list[i])
            elif temp_list[i].split(" : ")[0] == "":
                i+=1
            else:
                entry[key][temp_list[i].split(" : ")[0].strip()] = temp_list[i].split(" : ")[0].strip()

            # this might get called multiple times within block, but it'll have the same output each time
            if 'boxes' in entry[key] and 'answer_order' in entry[key]:
                entry[key]['boxes_in_order'] = get_ordered_converted_answers(entry[key]['boxes'], entry[key]['answer_order'])

            # fix because user file tutorial question ID doesn't match the IDs in the json files
            if 'question' in entry[key] and len(entry[key]['question']) == 1:
                entry[key]['question'] = "tm"+str(int(entry[key]['question'])+1)
                #print(entry[key]['question'])
    next_entry = str(PARTICIPANT_LIST['num_entries'])
    PARTICIPANT_LIST['num_entries'] = PARTICIPANT_LIST['num_entries'] + 1
    PARTICIPANT_LIST[next_entry] = entry


def return_type(list, full_string):
    if list[0].strip() == 'start':
        return int(list[1].strip()) 
    if list[0].strip() == 'finish':
        return int(list[1].strip())
    if list[0].strip() == 'boxes':
        return get_boxes(full_string)
    if list[0].strip() == 'answer_order':
        return get_answer_order(full_string)
    
    return list[1].strip()

def get_ordered_converted_answers(boxes, answer_order):
    new_list = [None]*4
    new_list[0] = boxes[get_index(0, answer_order)]
    new_list[1] = boxes[get_index(1, answer_order)]
    new_list[2] = boxes[get_index(2, answer_order)]
    new_list[3] = boxes[get_index(3, answer_order)]

    return new_list

def get_boxes(string):
    temp = string.split("boxes")[1].split("'")
    temp.pop(0)
    bool_list = [None]*4
    for x in range(0,4):
        temp[x*2] = temp[x*2].lower()
        
        if temp[x*2] == "true":
            bool_list[x] = 1
        else:
            bool_list[x] = 0

    return bool_list

def get_answer_order(string):
    temp = string.split("answer_order")[1].split("'")
    temp.pop(0)
    num_list = [None]*4
    for x in range(0,4):
        num_list[x]= int(float(temp[x*2]))

    return num_list

def get_index(offset, num_list):
    for x in range(0,4):
        if num_list[x]==offset:
            return x

def build_question_count(limit, bounds, key_set, status, filter):

    global PARTICIPANT_LIST
    global TRACK_ANSWERS

    # Processing filter action
    if not filter is None:
        filter = filter.strip()
    if not filter is None and not filter == "" and not filter == "all":
        filter_list = filter.strip().split(",")
        new_list = []
        #clean any leading/trailing whitespaces
        for filter_item in filter_list:
            new_list.append(filter_item.strip())
        filter_list = new_list
    else:
        filter_list = "all"

    if status == 'clear':
        #clear the answer stats.. don't append
        TRACK_ANSWERS = {}

    #final output
    question_list = {}

    counter = 0

    #print(PARTICIPANT_LIST)

    for k, user in PARTICIPANT_LIST.items():

        if k == 'num_entries':
            #not a user
            continue


        if bounds == 'upper':
            if user['session'] < limit:
                #skip it
                continue
        elif bounds == 'lower':
            if user['session'] > limit:
                #skip it
                continue
        else:
            #error
            continue

        # Apply filter
        if not check_filters(user, filter_list):
            continue
        
        print("session: "+str(user['session']) + ", Len: "+str(len(user.keys())) + ", key: "+k)
        # Go through each block
        for key_name, question_num in user.items():

            # gets question number as string
            try:
                key = question_num['question']
            except:
                #not a question
                continue

            # gets true/false list as 1's and 0's
            entries = question_num['boxes_in_order']

            index = check_answers(entries, key, key_set)
            if TRACK_ANSWERS.get(key, None) is None:
                #[0%, 25%, 50%, 75%, 100%]
                TRACK_ANSWERS[key] = [0,0,0,0,0]
            TRACK_ANSWERS[key][index] += 1


            if question_list.get(key, None) is None:
                #if question list is empty make it the default
                question_list[key] = entries
            else:
                question_list[key][0] += entries[0]
                question_list[key][1] += entries[1]
                question_list[key][2] += entries[2]
                question_list[key][3] += entries[3]
    
    return question_list
    
def check_filters(user, filter_list):
    
    #if "all" or an empty list, then it passes everything
    if "all" in filter_list or len(filter_list) == 0:
        return True

    for filter in filter_list:

        temp_list = filter.split(":")
        new_list = []
        #clear white space
        for item in temp_list:
            new_list.append(item.strip())
        temp_list = new_list

        key = temp_list[0]
        item = temp_list[1]

        for key_name, obj in user['bio'].items():
            #print(key_name)
            #print(obj)
            if key == key_name and item == obj:
                return True

    return False
    #for filter in filter_list:


def print_results(questions):
    global TRACK_ANSWERS
    keys = list(questions.keys())
    keys.sort()
    for key in keys:
        print ("question #: " + str(key) )
        print (questions[key])
        print ( str(TRACK_ANSWERS[key][0]) + " = 0%, ", 
                str(TRACK_ANSWERS[key][1]) + " = 25%, ", 
                str(TRACK_ANSWERS[key][2]) + " = 50%, ", 
                str(TRACK_ANSWERS[key][3]) + " = 75%, ", 
                str(TRACK_ANSWERS[key][4]) + " = 100%")
        total = TRACK_ANSWERS[key][0]
        total += TRACK_ANSWERS[key][1]
        total += TRACK_ANSWERS[key][2]
        total += TRACK_ANSWERS[key][3]
        total += TRACK_ANSWERS[key][4]
        print ("Total answers: "+str(total))
        print ("\n")

def load_answer_key(input):
    with open(input, encoding='utf-8') as data_file:
        answer_key = json.loads(data_file.read())


    #Note: Code below takes question objects from json and puts it in a list,
    # making a complete list of regular and tutorial question objects

    # this is a list of question objects
    # update added tutorial questions
    new_obj = answer_key['questions']
    for obj in answer_key['tutorial']:
        new_obj.append(obj)
    return new_obj

def get_answer_key(key, key_set):
    global ANSWER_KEY
    list = key_set
    for x in range(0,len(list)):
        if list[x]['question_id'] == key:
            return get_answer_list(list[x]['choices'])
    return False

def get_answer_list(question):
    correct_answers = [None]*4
    correct_answers[0] = (0,1)[question[0]['correct']=='yes']
    correct_answers[1] = (0,1)[question[1]['correct']=='yes']
    correct_answers[2] = (0,1)[question[2]['correct']=='yes']
    correct_answers[3] = (0,1)[question[3]['correct']=='yes']
    return correct_answers

def check_answers(list1, key, key_set):
    
    list2 = get_answer_key(key, key_set)
    if list2 == False:
        return False

    correct_answers = 0
    correct_answers += (0,1)[list1[0]==list2[0]]
    correct_answers += (0,1)[list1[1]==list2[1]]
    correct_answers += (0,1)[list1[2]==list2[2]]
    correct_answers += (0,1)[list1[3]==list2[3]]
    return correct_answers

ANSWER_KEY1 = load_answer_key('./master/static/multi_questions.json')
ANSWER_KEY2 = load_answer_key('./master/static/multi-full.json')

list_of_files = glob.glob('./anonymized-data-sc17-lab/*.txt')
num_read_file = read_file_group(list_of_files)
list_of_files2 = glob.glob('./anonymized-data-sc17-jig/*.txt')
num_read_file += read_file_group(list_of_files2)

file = open("participants.json","w")
file.write(json.dumps(PARTICIPANT_LIST))
file.close()

print("number of files read: "+str(num_read_file) + "\n")

questions = build_question_count(LOWER_BOUNDS, 'lower', ANSWER_KEY1, 'clear', 'exper: advanced')
print("\n\nLower. Filter: advanced")
print_results(questions)

questions = build_question_count(UPPER_BOUNDS, 'upper', ANSWER_KEY2, 'clear', 'exper: advanced')
print("\n\nUpper. Filter: advanced")
print_results(questions)

# Filtering: 
#
# key: string
# or 
# "all" or None for no filtering.
#
# example strings for "exper" key..
# exper: none, beginner, intermediate, advanced