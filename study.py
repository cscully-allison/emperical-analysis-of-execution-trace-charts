from flask import Flask
app = Flask(__name__)

from flask import render_template
from flask import request
import json
import time
import datetime
import os

@app.route("/end", methods=['POST'])
def endStudy():
    filename = request.form['session']
    curr_time = str(datetime.datetime.now())
    
    # this try block in a loop is to avoid a problem with
    # decoding JSON that pops up 
    while True:   # repeat until the try statement succeeds
        try:
            with open(filename+".json", 'r', encoding='utf-8') as openjson:
                studyinfo = openjson.read()
                if (studyinfo is not None) or (studyinfo is not ''):
                    curr_dict = json.loads(studyinfo)

            with open(filename+".txt", 'a') as savefile:
                savefile.write('End multi at:' + curr_time + '\n')

            if curr_dict is not None:
                curr_dict['bio']['end_multi'] = curr_time
                with open(filename+".json", 'w') as savejson:
                    savejson.write(json.dumps(curr_dict))

            return json.dumps({'status': 'OK'})
        except:
            print("JSON Decode Error. ")


@app.route("/comments/<session>")
def comments(session):
    print(session)
    return render_template('comments.html', session = session)

@app.route("/processComments", methods=['POST'])
def processComments():
    filename = request.form['session']
    savefile = open(filename+".txt", 'a')
    openjson = open(filename+".json", 'r')
    curr_dict = json.loads(openjson.read())
    openjson.close()
    curr_time = str(datetime.datetime.now())
    curr_dict['bio']['end_study'] = curr_time
    savejson = open(filename+".json", 'w')
    for k, v in request.form.items():
        print(k, v)
        curr_dict['bio'][k] = v
        savefile.write(k + ' : ' + v + '\n')
    savefile.write('End study at:' + curr_time + '\n')
    savefile.close()
    savejson.write(json.dumps(curr_dict))
    savefile.close()
    return thankyou()

@app.route("/thankyou")
def thankyou():
    return render_template('thankyou.html')


@app.route("/demographics")
def demographics():
    return render_template('demographics.html')

@app.route("/paper_proto")
def paper_proto(filename = "default.txt"):
    return render_template('drawing-template.html', session = filename)

@app.route("/tutorial")
def tutorial(filename=None):
    if filename == None:
        filename = request.args.get('filename')

    return render_template('tutorial.html', session = filename)

@app.route("/tutorialQuestions", methods=['POST'])
def tutorialQuestions():
    filename = request.form['session']
    savefile = open(filename+".txt", 'a')
    print(filename)
    return render_template('tutorial-multi.html', session=filename)

@app.route("/questions", methods=['POST'])
def questions():
    filename = request.form['session']
    savefile = open(filename+".txt", 'a')
    return render_template('multi.html', session=filename)

@app.route("/saveDemo", methods=['POST'])
def saveDemo():
    filename = str(time.time())
    
    savefile = open(filename+".txt", 'w')
    savejson = open(filename+".json", 'w')
    
    new_obj = { 'bio' : { 'answered': 0, 'version': '1.01' }, 'responses':[] }
    curr_time = str(datetime.datetime.now())
    new_obj['bio']['first_save'] = curr_time
    
    savefile.write('First save demo at:' + curr_time + '\n')
    for k, v in request.form.items():
        print(k, v)
        savefile.write(k + ' : ' + v + '\n')
        new_obj['bio'][k] = v
    savefile.write('version : 1.01')
    savefile.close()

    savejson.write(json.dumps(new_obj))
    savejson.close()
    return tutorial(filename)

@app.route("/grid")
def grid():
    return render_template('grid.html')

@app.route("/recordGrid", methods=['POST'])
def recordGrid():
    for k, v in request.form.items():
        print('k:', k, 'v:', v)
    return json.dumps({'status': 'OK'})

@app.route("/recordResponse", methods=['POST'])
def recordMulti():
    filename = request.form['session']

    savefile = open(filename+".txt", 'a')

    openjson = open(filename+".json", 'r')
    curr_dict = json.loads(openjson.read())
    openjson.close()

    savejson = open(filename+".json", 'w')
    savefile.write('--\n')
    
    index = int(curr_dict['bio']['answered'])
    curr_dict['bio']['answered'] = index + 1

    new_obj = {}
    for k, v in request.form.items():
        if k[-2:] == '[]':
            v = request.form.getlist(k)
            print('k:', k[:-2], 'v:', v)
            savefile.write(str(k[:-2]) + ' : ' + str(v) + '\n')
            liststring = str(v).replace("[","")
            liststring = liststring.replace("]","")
            newlist = liststring.split(", ")
            templist = []
            for entry in newlist:
                templist.append(entry[1:].replace("'","").strip())
            new_obj[str(k[:-2])] = templist
        else:
            new_obj[k] = v
            savefile.write(k + ' : ' + v + '\n')
            print('k:', k, 'v:', v)


    curr_dict['responses'].append(new_obj)
    savejson.write(json.dumps(curr_dict))
    
    savejson.close()
    savefile.close()
    
    return json.dumps({'status': 'OK'});

if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
