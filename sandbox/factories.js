var createSignaller = function(){
  var _signallers = []
  return {
    add: function(handlerFunction){_signallers.push(handlerFunction);},
    notify: function(args) {
      for(var i = 0; i < _signallers.length; i++)}{
        _signallers[i](args);
      }
    }
  };

};

var createModel = function(data){
  var _data = data;
  var _observers = createSignaller();
  return{
    register: function(s){
      _observers.add(s);
    }
  };
};
