var constants = {
  MIN_NUMBER_LINES:4,
  LINE_DENSITY_FACTOR: 40, //larger number = less dense
  MARGIN:{
    left: 40,
    right: 40,
    top: 0,
    bottom: 60
  },
  PADDING: 10,
  BLUR: 4,
  OPACITY: .8
}

var design_glyph_model = {
  type :"offset",
  angle: 15,
  grouped: true,
  color: "blue",
  overflow: {
    top: true,
    bottom: true
  }
}

//adapted from
// https://stackoverflow.com/questions/11394706/inverse-of-math-atan2
var get_y2_from_angle = function(x1,x2,y1,angle){
  theta = angle * Math.PI / 180;
  length = x2 - x1;
  dy = (length*.75 ) * Math.sin(theta);

  return dy+y1;
}

// Found at https://stackoverflow.com/questions/25405359/how-can-i-select-last-child-in-d3-js
d3.selection.prototype.first = function() {
  return d3.select(this.nodes()[0]);
};
d3.selection.prototype.last = function() {
  return d3.select(this.nodes()[this.size() - 1]);
};


/*
MASK DEFINITIONS
*/
var define_top_gradient_mask = function(){
  lin_grad = d3.select('defs')
              .append('linearGradient')
              .attr('id', 'top-gradient')
              .attr('y2', 1)
              .attr('x2', 0);

  lin_grad.append('stop')
          .attr('offset', 0.1)
          .attr('stop-color', 'white')
          .attr('stop-opacity', 0);

  lin_grad.append('stop')
          .attr('offset', 1)
          .attr('stop-color', 'white')
          .attr('stop-opacity', 1);

  mask = d3.select('defs')
          .append('mask')
          .attr('id', 'fade-up')
          .attr('maskContentUnits', 'objectBoundingBox');

  mask.append('rect')
        .attr('width', 1)
        .attr('height', 1)
        .attr('fill', 'url(#top-gradient)')

}

var define_bottom_gradient_mask = function(){
  lin_grad = d3.select('defs')
                .append('linearGradient')
                .attr('id', 'bottom-gradient')
                .attr('y2', 1)
                .attr('x2', 0);

  lin_grad.append('stop')
            .attr('offset', 0)
            .attr('stop-color', 'white')
            .attr('stop-opacity', 1);

  lin_grad.append('stop')
            .attr('offset', 0.9)
            .attr('stop-color', 'white')
            .attr('stop-opacity', 0);

  mask = d3.select('defs')
            .append('mask')
            .attr('id', 'fade-down')
            .attr('maskContentUnits', 'objectBoundingBox');

  mask.append('rect')
        .attr('width', 1)
        .attr('height', 1)
        .attr('fill', 'url(#bottom-gradient)');
}

var define_blur = function(){
  d3.select('defs')
      .append('filter')
      .attr('id', 'blur-def')
      .append('feGaussianBlur')
      .attr('in', 'SourceGraphic')
      .attr('stdDeviation', constants.BLUR);
}

var add_opacity_layer = function(svg, height, width){
  opacity_layer = svg.append('rect')
                          .attr('class', 'blur-opacity-layer')
                          .attr('height', height)
                          .attr('width', width)
                          .attr('fill', `rgba(255,255,255,${constants.OPACITY})`);
}


var define_clipping_mask = function(line_defs){

    var margin = 10;

    var path = `M${line_defs[0].x1}, ${line_defs[0].y1 - margin}
                L${line_defs[0].x2}, ${line_defs[0].y1 - margin}
                L${line_defs[0].x2}, ${line_defs[0].y2 - margin}
                L${line_defs[0].x1}, ${line_defs[0].y1 - margin}

                M${line_defs[line_defs.length - 1].x2}, ${line_defs[line_defs.length - 1].y2  + margin}
                L${line_defs[line_defs.length - 1].x1}, ${line_defs[line_defs.length - 1].y2  + margin}
                L${line_defs[line_defs.length - 1].x1}, ${line_defs[line_defs.length - 1].y1  + margin}
                L${line_defs[line_defs.length - 1].x2}, ${line_defs[line_defs.length - 1].y2  + margin}
                Z`;

    d3.selectAll('defs')
            .append('mask')
            .attr('id', 'abstracting-mask')
            .append('path')
            .attr('d', path)
            .attr('fill', 'white');
}


var get_num_lines = function(h){
  var num_lines;

  //calculate offsets by first getting number of lines
  if(h < 4*constants.LINE_DENSITY_FACTOR){
    num_lines = constants.MIN_NUMBER_LINES;
    h = 4*constants.LINE_DENSITY_FACTOR;
  }
  else {
    num_lines = h/constants.LINE_DENSITY_FACTOR;
  }

  return num_lines;
}

var define_down_lines = function(num_lines, h, scales, glyph_model){
  var line_defs = [];

  //define mathematical offsets as data array for d3
  for(var i = 0; i < num_lines; i++){
    var y2 = get_y2_from_angle(scales.x(0), scales.x(1), scales.y(i), glyph_model.angle);
    if (y2 < h){
      line_defs.push({"x1":scales.x(0), "y1":scales.y(i), "x2":scales.x(1), "y2":y2, id:"h"+i});
    }
  }

  return line_defs;
}

var define_up_lines = function(num_up_lines, down_lines, scales, glyph_model){
  up_lines = []
  dl_indx = down_lines.length - num_up_lines;
  for(var i = 0; i < num_up_lines; i++){
    up_lines.push({"x1":scales.x(0),"y1":down_lines[dl_indx+i].y2,"x2":scales.x(1),"y2":scales.y(i), id:"v"+i, color:glyph_model.color});
  }
  return up_lines
}

var flip_lines = function(lines){
  var flipped_lines = [];
  var id = 0;

  for(var line in lines){
    flipped_lines.push({"x1":lines[line].x1,"y1":lines[line].y2,"x2":lines[line].x2,"y2":lines[line].y1, id:"h"+id, color:lines[line].color});
    id ++;
  }

  return flipped_lines
}




//get the defintions of the lines as lists of x,y coords
var get_line_defs = function(glyph_model){
  var vert_lines = [];
  var horz_lines = [];
  var num_horz_lines = get_num_lines(glyph_model["height"]);
  var scales = {
    'x':d3.scaleLinear().domain([0,1]).range([0 + constants.MARGIN.left, glyph_model["width"] - constants.MARGIN.right]),
    'lines': d3.scaleQuantize().domain([15,60]).range([1,2,3,4])
  }
  var num_lines = scales['lines'](glyph_model.angle);
  scales['y'] = d3.scaleLinear().domain([0,num_horz_lines+num_lines]).range([0+constants.MARGIN.top, glyph_model["height"] - constants.MARGIN.bottom]);

  // get the line definitions for the downward sloping, left to right horizontal lines
  horz_lines = define_down_lines(num_horz_lines, glyph_model["height"], scales, glyph_model);

  // get the vertical lines for a ring sloping up from left to right
  if(glyph_model.type == 'ring'){
    vert_lines = define_up_lines(num_lines, horz_lines, scales, glyph_model);
  }
  // do a seperate calculation for exchanges
  else if (glyph_model.type == 'exchange') {
    // exchanges use a different number of lines by default
    scales['lines'].range([2,3,4,5]);
    num_lines = scales['lines'](glyph_model.angle);

    // vertical and horizontal lines are equal number
    scales['y'].domain([0, num_lines*2]);

    // need to define horizontal lines first before flipping to make the vertical lines look correct
    horz_lines = define_down_lines(num_horz_lines, glyph_model["height"], scales, glyph_model);
    vert_lines = define_up_lines(num_lines, horz_lines, scales, glyph_model);
    horz_lines = flip_lines(vert_lines);
  }

  return {up:vert_lines, down:horz_lines};
}


/**************************************
**** Primitive rendering defintions ***
***************************************/

var render_ring_primitives = function(primitive_groups, line_defs) {
  h_lines_grp = primitive_groups.selectAll('.down-lines-group');
  v_lines_grp = primitive_groups.selectAll('.up-lines-group');
  //enter
  h_lines = h_lines_grp.selectAll('.a-messages')
              .data(line_defs.down, d=>{return d.id})
              .enter()
              .append('line')
              .attr('class', 'a-messages')
              .attr('x1', (d)=>{return d['x1']})
              .attr('y1', (d)=>{return d['y1']})
              .attr('x2', (d)=>{return d['x2']})
              .attr('y2', (d)=>{return d['y2']});


  v_lines = v_lines_grp.selectAll('.a-messages')
              .data(line_defs.up, d=>{return d.id})
              .enter()
              .append('line')
              .attr('class', 'a-messages')
              .attr('x1', (d)=>{return d['x1']})
              .attr('y1', (d)=>{return d['y1']})
              .attr('x2', (d)=>{return d['x2']})
              .attr('y2', (d)=>{return d['y2']});

//update
  h_lines.attr('x1', (d)=>{return d['x1']})
        .attr('y1', (d)=>{return d['y1']})
        .attr('x2', (d)=>{return d['x2']})
        .attr('y2', (d)=>{return d['y2']});

  v_lines.attr('x1', (d)=>{return d['x1']})
        .attr('y1', (d)=>{return d['y1']})
        .attr('x2', (d)=>{return d['x2']})
        .attr('y2', (d)=>{return d['y2']});


//leave
  h_lines.exit().remove;
  v_lines.exit().remove;

  d3.selectAll('.up-lines-group')
    .attr('mask', 'url(#abstracting-mask)')
}

//rendering an offset primitive
var render_offset_primitive = function (primitive_groups, line_defs){
    h_lines_grp = primitive_groups.selectAll('.down-lines-group');

    h_lines = h_lines_grp.selectAll('.a-messages')
                .data(line_defs.down, d=>{return d.id})
                .enter()
                .append('line')
                .attr('class', 'a-messages')
                .attr('x1', (d)=>{return d['x1']})
                .attr('y1', (d)=>{return d['y1']})
                .attr('x2', (d)=>{return d['x2']})
                .attr('y2', (d)=>{return d['y2']});

  //update
    h_lines.attr('x1', (d)=>{return d['x1']})
          .attr('y1', (d)=>{return d['y1']})
          .attr('x2', (d)=>{return d['x2']})
          .attr('y2', (d)=>{return d['y2']});


    h_lines.exit().remove;

}


var render_exchange_primitive = function(primitive_groups, line_defs){
  h_lines_grp = primitive_groups.selectAll('.down-lines-group');
  v_lines_grp = primitive_groups.selectAll('.up-lines-group');
  
  //enter
  h_lines = h_lines_grp.selectAll('.a-messages')
              .data(line_defs.down, d=>{return d.id})
              .enter()
              .append('line')
              .attr('class', 'a-messages')
              .attr('x1', (d)=>{return d['x1']})
              .attr('y1', (d)=>{return d['y1']})
              .attr('x2', (d)=>{return d['x2']})
              .attr('y2', (d)=>{return d['y2']});


  v_lines = v_lines_grp.selectAll('.a-messages')
              .data(line_defs.up, d=>{return d.id})
              .enter()
              .append('line')
              .attr('class', 'a-messages')
              .attr('x1', (d)=>{return d['x1']})
              .attr('y1', (d)=>{return d['y1']})
              .attr('x2', (d)=>{return d['x2']})
              .attr('y2', (d)=>{return d['y2']});

//update
  h_lines.attr('x1', (d)=>{return d['x1']})
        .attr('y1', (d)=>{return d['y1']})
        .attr('x2', (d)=>{return d['x2']})
        .attr('y2', (d)=>{return d['y2']});

  v_lines.attr('x1', (d)=>{return d['x1']})
        .attr('y1', (d)=>{return d['y1']})
        .attr('x2', (d)=>{return d['x2']})
        .attr('y2', (d)=>{return d['y2']});


//leave
  h_lines.exit().remove;
  v_lines.exit().remove;

  d3.selectAll('.up-lines-group')
    .attr('mask', 'url(#abstracting-mask)');
}

// main primitive rendering function call
var render_primitives = function(elem, glyph_models, num_glyphs){
  //scales
  var svg_height = parseInt(elem.attr('height'))
  var x_scale = d3.scaleLinear().domain([0,1]).range([0 + constants.MARGIN.left, glyph_models[0]['width'] - constants.MARGIN.right]);
  var y_scale = d3.scaleLinear().domain([0,num_glyphs]).range([0, svg_height]);
  var primitive_groups = elem.selectAll('.glyph-area')
                              .data(glyph_models)
                              .enter()
                              .append('g')
                              .attr('class', 'glyph-area')
                              .attr('height', (d) => {return d['height']})
                              .attr('width', (d) => {return d['width']});

  if(glyph_models.length == 1){
    primitive_groups.attr('transform', (d) => {
      return `translate(${0}, ${y_scale(d['id']) + (.5*svg_height - .5*d['height'])})`;
    })
  }
  else{
    primitive_groups.attr('transform', (d) => {
      return `translate(${0}, ${y_scale(d['id']) + (constants.PADDING * d['id'])})`;
    })
  }

  primitive_groups.append('g')
                .attr('class', 'up-lines-group')
                .attr('height', (d) => {return d['height']})
                .attr('width', (d) => {return d['width']});
  primitive_groups.append('g')
                .attr('class', 'down-lines-group')
                .attr('height', (d) => {return d['height']})
                .attr('width', (d) => {return d['width']});


  //define clipping mask
  line_defs = get_line_defs(glyph_models[0]);
  define_clipping_mask(line_defs.down);

  //switch over possible primitives
  if(glyph_models[0].type == "offset"){
    render_offset_primitive(primitive_groups, line_defs);
  }
  if(glyph_models[0].type == "ring"){
    render_ring_primitives(primitive_groups, line_defs);
  }
  if(glyph_models[0].type == "exchange"){
    render_exchange_primitive(primitive_groups, line_defs);
  }

  return primitive_groups
}


//this is our setup area
// will be pushed to a function soon
window.addEventListener('load', ()=>{
  var element = d3.select(".draw-area");
  var def_h = element.node().offsetHeight - 50;
  var def_w = element.node().offsetWidth;
  var colors = ["black", "blue"]
  var models = [];


  if(design_glyph_model.grouped == true){
    //divide height by some constant
    // 5 times the line density factor (which is in pixels)
    var g_h = 5 * constants.LINE_DENSITY_FACTOR
    var num_glyphs = Math.round(def_h / (g_h+constants.PADDING));
    //build up and store 5 line glyph models
    //with alternating colors
    for(var i = 0; i < num_glyphs; i++){
      var temp = {};
      Object.assign(temp, design_glyph_model);
      temp['height'] = g_h;
      temp['width'] = def_w;
      temp['color'] = colors[i%2];
      temp['id'] = i;
      models.push(temp);
    }
  }
  else {
    var temp = {};
    var num_glyphs = 1;
    Object.assign(temp, design_glyph_model);
    if(design_glyph_model.type != "exchange"){
      temp['height'] = def_h;
    }
    else {
      temp['height'] = def_h*.5;
    }
    temp['width'] = def_w;
    temp['color'] = colors[0];
    temp['id'] = 0;
    models.push(temp);
  }

  var svg = element.append('svg')
                    .attr('id', 'svg-base')
                    .attr('height', def_h)
                    .attr('width', def_w);

  svg.append('defs');

  define_blur();

  // create some test boxes as background
  var test_boxes = []
  for(var i = 0; i < 20; i++){
    test_boxes.push({'id':i, 'height':100, 'width': def_w, 'color': 'blue'})
  }

  svg.append('g')
  .attr('filter', 'url(#blur-def)')
  .selectAll('.test-box')
  .data(test_boxes)
  .enter()
  .append('rect')
  .attr('class', 'test-box')
  .attr('height', (d)=>{return d.height})
  .attr('width', (d)=>{return d.width})
  .attr('fill', (d)=>{return d.color})
  .attr('transform', (d)=>{ return `translate(${0},${(d.height*d.id)+(10*d.id)})`});

  add_opacity_layer(svg, def_h, def_w);


  render_primitives(svg, models, num_glyphs);

  glyphs = svg.selectAll('.glyph-area');
  
  if(design_glyph_model.overflow.top == true){
    define_top_gradient_mask();
    glyphs.first()
          .attr('mask', 'url(#fade-up)');
  }
        
  if(design_glyph_model.overflow.bottom == true){
    define_bottom_gradient_mask();
    glyphs.last()
          .attr('mask', 'url(#fade-down)');
  }


})
