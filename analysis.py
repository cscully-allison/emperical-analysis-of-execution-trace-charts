import pandas as pd
import json

experiment_data = None

with open(r"1606956686.9586577.json", 'r+') as f:
    experiment_data = json.loads(f.read())

with open(r"static\test_questions.json", 'r+') as f:
    experiment_metadata = json.loads(f.read())


print(experiment_metadata['tutorial'][3])

responses = pd.DataFrame(experiment_data['responses'])

print(responses)
